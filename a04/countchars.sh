#!/bin/sh

[ $# -ge 1 ] || {
	cat >&2 <<- EOF
		NAME
		countchars.sh - counts the given characters on each line of input

		SYNOPSIS
		$(basename $0) letter[...]

		DESCRIPTION
		Counts occurrences of the given characters on each line of input.
		If given multiple characters, 1st char in column1, 2nd in col 2, etc.
		Columns delimited by space.
		Behaviour undefined if given arguments longer than a single character.

		EXAMPLES

		\$ seq 9 14 | ./countchars.sh 1  # counts number of '1's on each input line
		1:0
		1:1
		1:2
		1:1
		1:1
		1:1

		\$ seq 40 | { tr '\n' ' ' ; echo ; } | $0 '5' '2'
		5:4 2:14

		\$ {
		cat <<- USAGE
		abcdcba
		n1a2bb3ccc4dddd
		USAGE
		} | $0 'a' 'd' 'c' 'e'
		a:2 d:1 c:2 e:0
		a:1 d:4 c:3 e:0

		\$ printf -- '  6  spaces  \n' | $0 ' ' 's'
		 :6 s:2

		\$ head ~/environment/1280data/books/"Alice's Adventures in Wonderland, by Lewis Carroll.txt" | ./countchars.sh 'l'
		l:0
		l:0
		l:2
		l:0
		l:0
		l:0
		l:0
		l:0
		l:0
		l:1

		\$ cat ~/environment/1280data/books/"Alice's Adventures in Wonderland, by Lewis Carroll.txt" | ./countchars.sh 'a' 'A' '?' | shasum
		53745c9a6a3792c87df2ec6d6f29188d5ce1c77f  -
	EOF
	exit 1;
}

num=0;

while IFS='\n' read ch; 
do
  for k in $@; 
   do
     echo "$ch" | tr -dc $k | wc -m >> temp
     
     num=$(( $num+1 ))
 
     if [ $counter -eq $# ]; 
      then
        echo "," >> temp
        num=0;
     fi
    done;
done;

cat temp | tr '\n' ' ' | tr ',' '\n' 
rm temp