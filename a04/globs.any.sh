#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.any.sh - displays entries in the current directory that match any of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ANY of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched any glob
		1 if no name matched any glob
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*' '*z*'
		123
		abc
		abc123

	EOF
	exit 2
} >&2

#$*
f=""
num=0
gb=""

for gb in $*
do
  ls $gb>/dev/null 2>&1
  if [ $?==0 ] ; then
    num=$(( $num+1 ))
    f=$f""$gb
  fi
done

if [ $num -gt 0 ] ; then
   #display list
    echo $f | tr "" "\n"
    exit 0;
  else 
    exit 1;
fi