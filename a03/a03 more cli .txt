
Do not include name or student ID.

Submit via your handin repository on gitlab before the deadline.
Ensure that you have submitted the repository's URL via the d2l quiz.

For all of these questions, you may NOT use:
	grep, sed, awk, or perl
	if, for, while, until, case, or read

For all questions that provide a partial shasum, you must provide BOTH the command AND the shasum of the output.

----------------------------------------

q01) [16; 2 each] Give a GLOB (no grep or regular expressions) that matches filenames that:

	a) contain an 'a', 'b', or 'c'
     
     ans: ls -- *[abc]*
		

	b) contain some character that is not an 'a', 'b', or 'c'

	ans. ls -- *[!abc]*	

	c) contain an 'a', 'b', or 'c' at some position other than the first or last character
		eg: MATCH: aaa, acc, xax, xaa; NOT MATCH: axb, bxc

    ans. ls -- ?*[abc]*?
		

	d) contain an 'a', 'b', or 'c', but none of 'a', 'b', or 'c' are the first or last character
		eg: MATCH: xax, xabcx; NO MATCH: aabcx, xaa
    
	ans. ls -- [!abc]*[abc]*[!abc]

	e) contain at least 2 'a's
   
    ans. ls -- *[a]*[a]*		

	f) contain at least 2 'a's where there is exactly one non-'a' character between them

	ans. ls -- *[a][!aa]*

	g) contain at least 2 'a's with any number of any characters between them, but at least one character between them must not be an 'a'

	ans. ls -- *[a]*[!a]*[a]*	

	h) match log files created in January 2014, where filenames start with a date in ISO-8601 date format, and end in a '.log' extension.

	ans. find -iname "2014-01*.log"  -newermt 2017-01-01 ! -newermt 2017-01-30 -ls	


q02) [6] Give a command to output the difference between the number of lower case letters in the books corpus compared to the number of upper case characters in the books corpus. ie: how many more lower case letters there are than upper case letters. (http://mylinux.langara.ca/~jhilliker/1280/books.tar.bz2) [shasum begins: 9]

   ans. echo $(( $(cat * | tr -cd '[:lower:]' | wc -m)-$(cat * | tr -cd '[:upper:]' | wc -m) )) | shasum		


q03) [7] Give a command to display the average file size (rounded down) of the books corpus. Use only: cat, echo, ls, wc, and shell meta-characters. [shasum begins: 6]

   ans. echo $(( $(cat *| wc -c) / $(ls | wc -l))) | shasum		


q04) [10] Give a command to calculate the total file size of all the books in the books corpus (the sum of their file sizes). You may only use the following commands: cut, echo, eval, head, ls, tail, tr, and any shell meta-characters. Note: this should work for a variable number of files -- do not restrict it to only working with 15 files. [shasum begins: 8]

    ans. eval echo $(( $(ls -l | tr -s ' ' | cut -d ' ' -f5 | tr '\n' '+')  0 )) | shasum		
   

q05) [4]
	a) [3] Give a command that will display the contents of the variable "word" followed by the character "s", followed by the contents of the variable "words", followed by the string "$word". No spaces.
	eg: if word=foo and words=bar
	then the result should be: foosbar$word
	
	ans. echo "$word"s"$words"'$word'
	
	b) [1] Same as above, but handle the last element, "$word" literal, differently.

	ans. echo "$word"s"$words"\$word
		

q06) [8] Give a command that will run the program "work" on a weekday, and the program "play" on the weekend. Ensure that only one will run.

	ans. i)  * * * * 1-5 ./work
	     ii) * * * * 0,7 ./play

q07) [4] Suppose you are writing an automated test.  The following files exist:
	"foo", the program under test
	"foo.1.in", the test's stdin
	"foo.1.out.exp", the expected stdout
	"foo.1.err.exp", the expected stderr

	Run "foo" (only once) with the given input, and compare its actual outputs with the expected outputs.
	"foo" is expected to exit with success, and the outputs are expected to match.
	Always test all 3 conditions.
	Display the diff of the outputs if they do not match.
	Exit failure if any of the 3 tests fail.
	Use "foo.1.out.act" and "foo.1.err.act" as temporary files to hold the actual outputs of foo.

		


q08) [6] the file "a03.q08.txt" contains two columns of numbers. Give a command that will list the numbers (in ascending order) that appear in both columns. Do not use temporary files. Use process substitution. [shasum begins: 4]
 
   ans.   sort -t' ' -k1,1 -k2,2n a03.q08.txt | shasum
              
                   or 
               
          cat a03.q08.txt | sort -k1 -k2 | shasum
          
          
q09) [6]
	a) [4] The file "a03.q09.d3" [shasum begins: 30] is the result of running "diff -u1 a03.q09.d1 a03.q09.d2 > a03.q09.d3". Use this information to reconstruct the reported region of "a03.q09.d1" [shasum begins: e] and "a03.q09.d2" [shasum begins: 7]. Submit the 3 files, and give their shasums here. (run: shasum -t a03.q09.d?).
      
       ans. i) for a03.q09.d1
            ecc72cbb18b3ee1237ebe93d608e10b834152474
            
            ii) for a03.q09.d2 
            7c74002127ee01bb9794516ea5c4389a24124d79
            
            iii)for a03.q09.d3
            30c9e9236c002b740f087c612213fe769ef4b432
		

	b) [2] Does "a03.q09.d3" hold enough information to reconstruct all of "a03.q09.d1" and "a03.q09.d2"? Briefly explain why or why not.

     ans. this file hold the enough info to reconstruct all of "a03.q09.d1" and "a03.q09.d2". because the minus and plus sign helps to reconstruct them. the information help to find them. we can change them and rename and still can find difference .


q10) [10; 2 each] Run "labFiles.bash" to set up the test directory. You must pipe the output of find into sort to get the correct shasum.  NOTE: labfiles.sh has changed since the lab where it was used.
	eg:
	$ rm -rf tmp && labFiles.bash
	> b5c7c5026aca1ed26fcdd317e7f962f21473b632  -

	$ cd tmp && find | sort | shasum
	> 4656d98aea254849a2e20797403691e4cb8098f8  -

	Give a find command to display all of:

	a) the files larger than 2 kilobytes. [shasum begins: 6]

	 ans.  find . -type f -size +2k | shasum

	b) the files last modified more than 1 year ago (assume 365 days per year). [2]

	 ans. find . -type f -mtime +365 | shasum	

	c) the files last modified between 6 and 12 months ago (assume 30 days per month). [7]

	ans. find . -type f -mtime +180 -a -mtime -365 | shasum

	d) the 2nd level directories (subdirectories of directories). [a]

	ans. find -maxdepth 2 -type d	

	e) the files that contain an "e" in their name, that also are in or under a directory with an "o" in it's name. [2]

    ans. find . -type f -name 'e'	
         find . -type d -name 'o'
