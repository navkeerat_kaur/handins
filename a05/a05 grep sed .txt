
*****
* Answer all questions with respect to the books corpus ( ~/environment/1280data/books/* )
* Assume questions are case-sensitive unless told otherwise (or to convert case).
* Each question is accompanied by a hex number that is the first digit(s) of the shasum of the correct output.
* Include the specified command, and the shasum of the output.

eg:
0) Display all lines that start with the substring "the". [0]

	cat * | grep "^the" | shasum
	092dda5fe38fe9367e8da9bab3ac231f7b8b6ab6  -


[20; 2 each] ***** PART 1) GREP
*** In the GREP section, you may only use CAT and GREP, unless stated otherwise.


1-3) Using BREs, give 3 different ways to display all lines that start with the substring "the", but not the word "the". [7]

  ans.
    1) cat * | grep "^the" | grep -wv "^the"  | shasum 
    2) cat * | grep "^the\w" | shasum
    3) cat * | grep "^the[[:alpha:]]" | shasum
	
     7793c9057da7a55b53811289d561f5f8808c7961  -

4) Using BRE(s), display all lines that start with a "t", but not with the substring "the" [d]

	ans. cat * | grep "^t" | grep -v "^the" | shasum

        d371225c1064457d4e8fea58f240869f1871e7d5  -

5) Using BREs, display all lines that contain the word "the", but do not contain the word "the" at the beginning or end of the line. [2]
	eg: REJECT "the the x", "x the the"
	eg: ACCEPT "x the x", "them the there"

  ans.  cat * | grep -wv '^the' | grep -w 'the' | grep -wv 'the$' | shasum
     
       09e81cc9a7a2b3a6ca1bc8d1a94cff76ffa5ef5  -

6) Using BREs, display all lines that contain an occurrence of the word "the" that is not at the beginning or end of a line. [0]
	eg: REJECT "the x", "x the"
	eg: ACCEPT "x the x", "the the x" "x the the", "the the the"
  ans.  cat * |grep -wv ^the | grep -wv the$ | grep -w the | shasum
	
	09e81cc9a7a2b3a6ca1bc8d1a94cff76ffa5ef52  -


7) Display all lines that contain the substring "the" that is not the word "the". [5]
	eg: ACCEPT "mother", "then the", "the thermos"
	EG: REJECT "the", "the a and the b"

 ans. cat * | grep "[a-z]the[a-z]" | grep -wv "the" | shasum
      
    50c84650fe5d54729a472f4f04900401897efc54  -

8) Only display the lines before and after a line that contains the substring "the", but do not display any lines that contain the substring "the". Do not display line grouping seperators. [e9]

 ans. cat * | grep -A 1 -B 1 the | grep -v the | shasum
  
      e5336b7e35c3198d2f47cff7995547430cb2b4f3  -
      
9) Using only grep and cut (no cat), display every 5th line of the entire corpus (not of the books individually), starting with line 1. eg: line 1,6,11,16,... No cat, awk, or sed. [27]

  ans. cat * | grep -n '.*' | grep -Eve  '^[2345789]$' -ve '[2345789]$' | cut -d: -f2 | shasum  
     
     29c6359f357882e4319f4aa449afc71cea030c0c  -

10) Display the word that most frequently precedes (comes before) the word "the". Use only cat, grep, sort, uniq.  No awk, sed, cut, head, or tail. [c8]

 ans. cat * | grep -Eo "$(cat * | grep -o "\w*" | sort | uniq -c | sort -n | tail -1 | cut -d " " -f4 )\W+\w+\W+" | grep -E '.*the'|shasum
  
       2a15caa10e4a3ce43cfb29b3d10acea148784c7d  -

[20; 2 each] ***** Part 2) SED
*** In the SED section, you may only use CAT and SED unless stated otherwise.


11) Display every 5th line of the entire corpus (not of the books individually), starting with line 1. [27]

  ans. cat * | sed -n '1~5p' | shasum	
  
       272416f803f75cd9baff20d55b95acb17ab45ed0  -


12) Replace every word with a number of asterisks equal to its length. [4]
	eg: "and that she has counted me, among the dead."
	->  "*** **** *** *** ******* **, ***** *** ****."

  ans. cat *| sed  's/\w/*/g' | shasum  

       4c2887b28ed51402de11431b7738a34c5c0ff5dd  -
       
13) Replace every occurrence of the word "good" with "better" and "better" with "best". [c]
	eg: "good, better, best. never let it rest. 'til your good is better and your better is best."
	-> "better, best, best. never let it rest. 'til your better is best and your best is best."

	ans. cat *|  sed  's:\bbetter\b:best:g' | sed 's:\bgood\b:better:g' | shasum

        c0ea7780319d14a5f50d431fb087029e956c63a6  -
        
        
14) On every line that does not contain the substring "safe", replace all occurrences of the substring "out" with "OUT". [4]


 ans. cat * |  sed -n  '/safe/!p' | sed 's/out/OUT/g' | shasum
   
      4c1cf657d2025aad7da620dd0cd27e90ea429baf  -

15) Place the line "BEFORE" before every line containing the word "before", and the line "AFTER" after every line containing the word "after" [no quotes]. Do not use 's/' [6]

 ans. cat * |  sed  '/\bafter\b/ s/^/--ABOVE--\n/g' | sed '/\bbefore\b/ s/$/\n--BELOW--/g' | shasum 

     6205b87f676e0379ab8dc3ff87c515819d8cf7df  -

16) Remove all characters within parenthesis (on the same line). Take the outermost parenthesis. [9]
	eg: "a (b (c) d) e" -> "a () e"
	eg: "a (b) (c) (d) e" -> "a () e"
	eg: "a ((((((b) c" -> "a () c"
	eg: "a (b" -> "a (b"

	ans. cat * | sed '/[a-zA-Z0-9] (/s//-/' | shasum
	
       85f5d454eef754baf0392e61254c5378f426f15a  -

17) For all lines where the same word occurs twice in a row (with an non-zero number of non-word characters between them), replace the second occurrence with the word 'jinx'. [02]

 ans. cat * | sed -r 's/\b(\w+)(\W+)\1\b/\1\2\jinx/g' | shasum
 
      023d8ec9acd7c069b7961b98ee0ede53e194433d  -


18) Swap the first and last word on every line. [c0]
	eg: ' "Hi how are you?" ' -> ' "you how are Hi?" '

 ans. cat * | sed -r 's/^([^ ]*) (.*) ([^ ]*)$/\3 \2 \1/' | shasum 
   
     c0ba6d0e65f71c698236d1c7bee8c7c6646af845  -

19) Double every word that contains "two" as a substring, but do not double the word "two" itself. Put one space between them. [a5]
	eg: 'two networks of driftwood for two trustworthy twopence'
	-> 'two networks networks of driftwood driftwood for two trustworthy trustworthy twopence twopence'

  ans. cat * | sed -r 's:\b((\w*)two(\w+)|(\w+)two(\w*))\b:\1 \1:g' | shasum

     a5aa1d7070a1240c462466a75a0fbbe31dd52d16  -
       

20) Remove the line that follows any line that contains the word "missing". [8b]
	eg: 'a\nmissing\nb\nc\n' -> 'a\nmissing\nc\n'

 ans. cat * | sed  '/\bmissing\b/{n; d}' | shasum

      8b712058c5ac4c5ee45bb18862c185c387de44a7  -

[10; 2 each] ****** PART 3) REs

State if the following pairs of EREs are the same or different. If they are the same, describe the language which they accept. If they are different, give a counter-example which is accepted by one of them, but not the other, and state which ERE accepts and which ERE rejects.

eg 1:
    a.*b
    ab

	DIFFERENT
	"acb"
	1st accepts
	2nd rejects

eg2:
    a.*
    a+

	SAME
	"Contains an 'a'"


21) (ab)*
    a*b*
 
  ans. SAME
  
   contains "ab" or not "ab"
   
   to explain more:
   contains 'a' and 'b' either together like "cab" or seperate like "casssb" and "casssb" is equal to zero times '(ab)' string.


22) (ab)+
    a+b+

   ans. SAME
        
        contains at least one time the string "ab" in a row
	


23) ab[^b]*
    ab{1,4}

   ans. SAME 
     
        contains the string "ab" in row


24) (ab?)+(a?b)+
    (b?a)+(ba?)+
   
   ans. SAME
       
	   contains string "ab" in a row.


25) ab[^a]*[^b]*$
    ab[^ab]*$
    
    ans. NOT SAME
         
         ex:- abnormal
              1st accepts
              2nd rejects

	
